<?php
$I = new Step\Acceptance\CRMGuestSteps($scenario);
$I->wantTo('check Guest-level access rights');

// Check Customers
$I->amOnPage('/customers/index');
$I->seeElement('#login-form');

$I->amOnPage('/customers/query');
$I->seeElement('#login-form');

$I->amOnPage('/customers/add');
$I->seeElement('#login-form');

// Services
$I->amOnPage('/services/index');
$I->seeElement('#login-form');

$I->amOnPage('/services/view?id=1');
$I->seeElement('#login-form');

$I->amOnPage('/services/create');
$I->seeElement('#login-form');

// Users
$I->amOnPage('/users/index');
$I->seeElement('#login-form');

$I->amOnPage('/users/view?id=10');
$I->seeElement('#login-form');

$I->amOnPage('/users/create');
$I->seeElement('#login-form');
