<?php
$I = new Step\Acceptance\CRMUserSteps($scenario);
$I->wantTo('check User-level access rights');

// Check Customers
$I->amOnPage('/customers/index');
$I->dontSee('Forbidden');

$I->amOnPage('/customers/query');
$I->dontSee('Forbidden');

$I->amOnPage('/customers/add');
$I->see('Forbidden');

// Services
$I->amOnPage('/services/index');
$I->see('Forbidden');

$I->amOnPage('/services/view?id=1');
$I->see('Forbidden');

$I->amOnPage('/services/create');
$I->see('Forbidden');

// Users
$I->amOnPage('/users/index');
$I->see('Forbidden');

$I->amOnPage('/users/view?id=10');
$I->see('Forbidden');

$I->amOnPage('/users/create');
$I->see('Forbidden');
