<?php
$I = new \Step\Acceptance\CRMUserSteps($scenario);
$I->wantTo('see whether user documentation is accessible');

$I->amInDocsPage();
$I->seeTitle();
$I->seeLargeBodyOfText();
