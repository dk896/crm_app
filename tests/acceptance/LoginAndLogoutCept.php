<?php

$I = new Step\Acceptance\CRMUsersManagementSteps($scenario);
$I->wantTo('check that login and logout work');

$I->amGoingTo('register new User');

$I->clickOnUsersListLink();
$I->seeUsersListLink();
$I->seeCreateUserLink();

$I->clickOnCreateUserRecordLink();
$I->wait(3);

$I->seeIamInCreateUserUi();
$user = $I->imagineUser();
$I->fillUserDataForm($user);

$I->submitUserDataForm();
$I->clickOnAsideLogout();

$I = new Step\Acceptance\CRMGuestSteps($scenario);
$I->amGoingTo('login existing user');

$I->seeLink('Login', '/site/login');
$I->clickOnAsideLogin();
$I->wait(2);

$I->seeIAmInLoginFormUi();
$dbUser['UserRecord[username]'] = 'veniam_reiciendis';
$dbUser['UserRecord[password]'] = 'veniam_reiciendis';
$I->fillLoginForm($dbUser);
$I->submitLoginForm();
$I->wait(2);

$I->seeIamAtHomepage();
$I->dontSee('Login', '/site/login');

$I->seeUsername($dbUser);
$I->seeLink('Logout', '/site/logout');

$I->clickOnAsideLogout();

$I->seeIamAtHomepage();
$I->dontSeeUsername($dbUser);
$I->dontSeeLink('Logout', '/site/logout');
$I->seeLink('Login', '/site/login');
