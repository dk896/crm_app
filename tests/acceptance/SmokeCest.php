<?php

class SmokeCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    public function homePageWorksTest(AcceptanceTester $I)
    {
        $I->wantTo('See that home page is up');
        $I->amOnPage('/');
        $I->see('Welcome to Client Record Manager');
        $I->wait(2);
    }
}
