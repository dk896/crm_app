<?php
$I = new \Step\Acceptance\CRMServicesManagementSteps($scenario);
$I->wantTo('edit existing Service record');

$I->amInListServicesUi();
$I->clickOnRegisterNewServiceButton();
$I->seeIAmInAddServiceUi();

$first_service = $I->imagineService();
$I->fillServiceDataForm($first_service);
$I->submitServiceDataForm();

$I->seeIamInViewServiceUi();
$I->see($first_service['ServiceRecord[name]'], '//*[@id="w0"]/tbody/tr[2]/td');
$I->see($first_service['ServiceRecord[hourly_rate]'], '//*[@id="w0"]/tbody/tr[3]/td');

$I->click('//div/div[1]/section[1]/ul/li[2]/a[@href="/services/index"]');
$I->seeIAmInListServicesUi();

$I->seeEditButtonBesideService($first_service);
$I->clickEditButtonBesideService($first_service);
$I->seeEditServiceUi($first_service);

$new_data = $I->imagineService();
$I->fillServiceDataForm($new_data);
$I->submitServiceDataForm();

$I->amInListServicesUi();
$I->seeServiceInList($new_data);
$I->dontSeeServiceInList($first_service);
