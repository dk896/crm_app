<?php
$I = new Step\Acceptance\CRMOperatorSteps($scenario);
$I->wantTo('check Manager-level access rights for operator management');

// Check Customers
$I->amOnPage('/customers/index');
$I->dontSee('Forbidden');

$I->amInQueryCustomerUi();
$I->dontSee('Forbidden');

$I->amInAddCustomerUi();
$I->dontSee('Forbidden');

// Services
$I->amOnPage('/services/index');
$I->dontSee('Forbidden');

$I->amOnPage('/services/view?id=1');
$I->dontSee('Forbidden');

$I->amOnPage('/services/create');
$I->dontSee('Forbidden');

// Users
$I->amOnPage('/users/index');
$I->see('Forbidden');

$I->amOnPage('/users/view?id=10');
$I->see('Forbidden');

$I->amOnPage('/users/create');
$I->see('Forbidden');

$I->logout();


$I = new Step\Acceptance\CRMServicesManagementSteps($scenario);
$I->wantTo('check Manager-level access rights for Services management');
// Check Customers
$I->amOnPage('/customers/index');
$I->dontSee('Forbidden');

$I->amOnPage('/customers/query');
$I->dontSee('Forbidden');

$I->amOnPage('/customers/add');
$I->dontSee('Forbidden');

// Services
$I->amOnPage('/services/index');
$I->dontSee('Forbidden');

$I->amOnPage('/services/view?id=1');
$I->dontSee('Forbidden');

$I->amOnPage('/services/create');
$I->dontSee('Forbidden');

// Users
$I->amOnPage('/users/index');
$I->see('Forbidden');

$I->amOnPage('/users/view?id=10');
$I->see('Forbidden');

$I->amOnPage('/users/create');
$I->see('Forbidden');
