<?php

use app\common\fixtures\ServiceFixture;

class ServicesListApiTest extends \Codeception\Test\Unit
{
    /**
     * @var \ApiTester
     */
    protected $tester;
    
    protected function _before()
    {
        $this->tester->haveFixtures([
            'service' => [
                'class' => ServiceFixture::class,
                'dataFile' => codecept_data_dir() . 'service.php',
            ]
        ]);
    }

    protected function _after()
    {
    }

    /** @test */
    public function testHasJsonEndpoint()
    {
        $this->tester->sendGET('/api/services/json');
        $response = $this->tester->grabResponse();

        $this->tester->canSeeResponseCodeIs(200);
        $this->tester->canSeeResponseIsJson();

        $this->assertNotEquals('', $response);
    }

    /** @test */
    public function testReturnsValidJson()
    {
        $this->tester->sendGET('/api/services/json');
        $this->tester->canSeeResponseCodeIs(200);

        $this->tester->seeResponseContainsJson([
            [
                'id' => 1,
                'name' => 'Et non aspernatur maiores.',
                'hourly_rate' => 41,
            ],
            [
                'id' => 2,
                'name' => 'Quas omnis eaque.',
                'hourly_rate' => 46,
            ],
        ]);
    }

    private function registerService()
    {
        $service = $this->imagineService();
        $service->save();

        return $service->attributes;
    }

    private function imagineService()
    {
        $faker = \Faker\Factory::create();

        $service = new \app\models\service\ServiceRecord();
        $service->name = $faker->sentence($word = 3);
        $service->hourly_rate = $faker->randomNumber($digits = 2);

        return $service;
    }
}
