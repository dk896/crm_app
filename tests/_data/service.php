<?php

return [
    [
        'id' => 1,
        'name' => 'Et non aspernatur maiores.',
        'hourly_rate' => 41,
    ],
    [
        'id' => 2,
        'name' => 'Quas omnis eaque.',
        'hourly_rate' => 46,
    ],
    [
        'id' => 3,
        'name' => 'Magnam laborum ex nobis.',
        'hourly_rate' => 1,
    ],
    [
        'id' => 4,
        'name' => 'Mollitia non ab commodi.',
        'hourly_rate' => 47,
    ],
];
