-- MySQL dump 10.16  Distrib 10.3.9-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: d_crmapp
-- ------------------------------------------------------
-- Server version	10.3.9-MariaDB-1:10.3.9+maria~bionic-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_assignment`
--

DROP TABLE IF EXISTS `auth_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  KEY `auth_assignment_user_id_idx` (`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_assignment`
--

LOCK TABLES `auth_assignment` WRITE;
/*!40000 ALTER TABLE `auth_assignment` DISABLE KEYS */;
INSERT INTO `auth_assignment` VALUES ('admin','11',1533899667),('manager','10',1533899667),('user','9',1533899667);
/*!40000 ALTER TABLE `auth_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item`
--

DROP TABLE IF EXISTS `auth_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item`
--

LOCK TABLES `auth_item` WRITE;
/*!40000 ALTER TABLE `auth_item` DISABLE KEYS */;
INSERT INTO `auth_item` VALUES ('admin',1,'Can do anything including managing users',NULL,NULL,1533899667,1533899667),('guest',1,'Nobody',NULL,NULL,1533899667,1533899667),('manager',1,'Can manage entities in database, but not users',NULL,NULL,1533899667,1533899667),('user',1,'Can use the query UI and nothing else',NULL,NULL,1533899667,1533899667);
/*!40000 ALTER TABLE `auth_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item_child`
--

DROP TABLE IF EXISTS `auth_item_child`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item_child`
--

LOCK TABLES `auth_item_child` WRITE;
/*!40000 ALTER TABLE `auth_item_child` DISABLE KEYS */;
INSERT INTO `auth_item_child` VALUES ('admin','manager'),('manager','user'),('user','guest');
/*!40000 ALTER TABLE `auth_item_child` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_rule`
--

DROP TABLE IF EXISTS `auth_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_rule`
--

LOCK TABLES `auth_rule` WRITE;
/*!40000 ALTER TABLE `auth_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `notes` text DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_created_by` (`created_by`),
  KEY `customer_updated_by` (`updated_by`),
  CONSTRAINT `customer_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `customer_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES (1,'Ollie Witting','1975-02-01','Earum magnam et ad illo assumenda consequuntur quaerat.',NULL,NULL,NULL,NULL),(2,'Miss April Dibbert','1983-03-18','Perspiciatis nam inventore vel nobis placeat pariatur velit.',NULL,NULL,NULL,NULL),(3,'Prof. Johann Fahey','1972-06-19','Quia sapiente quia eaque nihil iure accusamus autem est.',NULL,NULL,NULL,NULL),(4,'Dr. Rosanna Schmidt I','1986-07-21','Sint eos repellendus sapiente distinctio architecto.',NULL,NULL,NULL,NULL),(5,'Goerge Clowny','1986-05-09','Best Goerge C',NULL,NULL,NULL,NULL),(6,'Serena Rohan','1986-07-05','Labore et rerum aperiam fugiat molestiae voluptates eligendi.',NULL,NULL,NULL,NULL),(7,'Lola Mayer','1975-01-07','Distinctio quia vitae sit nobis id et qui dolorem architecto.',NULL,NULL,NULL,NULL),(8,'Dr. Domenic Greenfelder','2016-09-15','Velit iure quo ullam labore eos et consectetur debitis molestias et.',NULL,NULL,NULL,NULL),(9,'Kelvin Jaskolski','1992-09-08','Qui vero alias quasi consequatur id natus laudantium qui.',NULL,NULL,NULL,NULL),(10,'Jack Goyette V','1992-11-08','Quidem explicabo dolores sequi et quia quia qui pariatur molestiae sit.',NULL,NULL,NULL,NULL),(11,'Anais Schimmel III','1994-01-21','Fugiat excepturi nisi et tempore numquam dolor expedita non.',NULL,NULL,NULL,NULL),(12,'Domenico Becker','1996-10-03','Porro incidunt quam molestiae nostrum molestiae facere quis.',NULL,NULL,NULL,NULL),(13,'Marianne Grant','1970-11-06','Rerum aut ex aut iusto temporibus esse eum magni odit.',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES ('m000000_000000_base',1530363833),('m140506_102106_rbac_init',1533893377),('m170907_052038_rbac_add_index_on_auth_assignment_user_id',1533893377),('m180521_185955_init_customer_table',1530363837),('m180521_190647_init_phone_table',1530363837),('m180523_132454_init_service_table',1530363838),('m180530_102208_init_user_table',1530363838),('m180806_063439_add_column_auth_key_to_user_table',1533537556),('m180810_064300_add_predefined_users_to_user_table',1533885917),('m180810_105837_create_roles_for_predefined_users',1533899667),('m180820_153221_add_audit_fields_to_customer',1534779517);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phone`
--

DROP TABLE IF EXISTS `phone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `number` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `customer_id` (`customer_id`),
  CONSTRAINT `customer_phone_numbers` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phone`
--

LOCK TABLES `phone` WRITE;
/*!40000 ALTER TABLE `phone` DISABLE KEYS */;
INSERT INTO `phone` VALUES (1,1,'874-844-7419'),(2,2,'+1.780.919.7498'),(3,3,'+16754280816'),(4,4,'(608) 410-4381 x58361'),(5,5,'+7-968-651-22-91'),(6,6,'(431) 647-8982 x3245'),(7,7,'728-939-0066'),(8,8,'(990) 863-1535 x148'),(9,9,'1-984-926-9662 x03310'),(10,10,'682-773-3356 x097'),(11,11,'324.336.4191 x6321'),(12,12,'1-615-592-2337'),(13,13,'1-554-791-1224');
/*!40000 ALTER TABLE `phone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `hourly_rate` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service`
--

LOCK TABLES `service` WRITE;
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
INSERT INTO `service` VALUES (1,'Super service #One',66),(2,'Service Quas omnis eaque.',47),(3,'Magnam laborum ex nobis.',1),(4,'Mollitia non ab commodi.',47),(5,'Accusantium ut tenetur.',61),(6,'Reiciendis saepe sint voluptatem.',32),(7,'Quidem odit consectetur accusamus.',89),(8,'Rerum veniam mollitia ullam.',32),(9,'Voluptates quo suscipit sunt voluptatibus.',82),(10,'Quaerat et repudiandae.',79),(11,'Ipsam possimus est dolores.',80),(12,'Beatae perspiciatis doloribus.',31);
/*!40000 ALTER TABLE `service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `auth_key` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `auth_key` (`auth_key`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'hechiddect','$2y$13$b7/164fC1k7T40qFSiMB0ee0w1MkCvbc3t47CgaTtcwyqsDyl36xm','164fC1k7T40qFSiMB0ee0w1MkCvbc3t47CgaTtcwyqsDyl36xm'),(2,'veniam_reiciendis','$2y$13$VBasYBesN3aogdX6N5Yte.gshLJhPK85vaMdJ8/cIy6S9.HOf2/Mi','VBasYBesN3aogdX6N5Yte.gshLJhPK85vaMdJ8/cIy6S9.HOf2/Mi'),(3,'et_alias','$2y$13$uX0EhssuPC8Z5bSEg5e6AudZFW8xs.AQYdS0aI8oStniTHBkPkgt2','uX0EhssuPC8Z5bSEg5e6AudZFW8xs.AQYdS0aI8oStniTHBkPkgt2'),(4,'eligendi_santus','$2y$13$aLaMbwYuBXDYWyzLTZt7MO8tvouDPyCEzeavHhtGurchwWqJtJY6q','aLaMbwYuBXDYWyzLTZt7MO8tvouDPyCEzeavHhtGurchwWqJtJY6q'),(5,'eti_samenuaturu','$2y$13$XulFOpXk43ebjGt9Yt0tueEtakRoICx3MjUV04bWA/2YwuRb./SN.','XulFOpXk43ebjGt9Yt0tueEtakRoICx3MjUV04bWA/2YwuRb./SN.'),(6,'beati_consequtor','$2y$13$M1L4Wss6pocv6co/EFClHu5N.9vBa8W39QA5dHEqHaz5qC9LZY7CC','M1L4Wss6pocv6co/EFClHu5N.9vBa8W39QA5dHEqHaz5qC9LZY7CC'),(7,'temper_queius','$2y$13$gnN0Ku5719tauwe38qNpTO7NcX7ar/DEdQGuCqXsX.CiRfEmjnZ7W','gnN0Ku5719tauwe38qNpTO7NcX7ar/DEdQGuCqXsX.CiRfEmjnZ7W'),(8,'dacimus_laborus','$2y$13$3nT4XWHNGSCG0O.dd3PyAOxq85eveC8namThV0J3zu.UvS0sXzuau','3nT4XWHNGSCG0O.dd3PyAOxq85eveC8namThV0J3zu.UvS0sXzuau'),(9,'JoeUser','$2y$13$1BK9D4PfxEobg1opoAi/huOTLN76UwnPVE8f27Pd4ue7WIp6I9pRm','GomYsdJdq8tVMWkY6Nht6S-rrhqMLY4s'),(10,'AnnieManager','$2y$13$yGshDbjJFLyjNXHKwT/kIuj6YkIkhxg87NxqICAB07I1aZUU.jVia','Fh4_VprFEQscdmiVPsEbL_BfKJmFKaqv'),(11,'RobAdmin','$2y$13$jxYlJ15pl4cp..fPy2moo.74f3VuuIN870E7r/gmdC99PbfMWSkEW','XV3XAvvup_azvdWgK1keS2YP2yEMXSes');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'd_crmapp'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-20 18:40:15
