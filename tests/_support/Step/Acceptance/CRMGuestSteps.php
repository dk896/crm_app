<?php

namespace Step\Acceptance;

use \Codeception\Util\Locator;

class CRMGuestSteps extends \AcceptanceTester
{
    public $username;
    public $password;

    public function __construct($scenario)
    {
        parent::__construct($scenario);

        if ($this->username && $this->password) {
            $this->login($this->username, $this->password);
        }
    }

    public function login($username, $password)
    {
        $I = $this;
        $I->amOnPage('/site/login');
        $I->fillField('LoginForm[username]', $username);
        $I->fillField('LoginForm[password]', $password);
        $I->click('//*[@id="login-form"]//button[@type="submit"]');
        $I->wait(2);
        $I->seeCurrentUrlEquals('/');
    }

    public function loginByAsideLink($username, $password)
    {
        $I = $this;
        $I->amOnPage('/');
        $I->click('//aside[1]/section/ul/li/a[@href="/site/login"]');
        $I->fillField('LoginForm[username]', $username);
        $I->fillField('LoginForm[password]', $password);
        $I->click('//*[@id="login-form"]//button[@type="submit"]');
        $I->wait(2);
        $I->seeCurrentUrlEquals('/');
    }

    public function logout()
    {
        $I = $this;
        $I->amOnPage('/');
        $I->click('//aside[1]/section/ul/li/a[@href="/site/logout"]');
    }

    public function clickOnAsideLogout()
    {
        $I = $this;
        $I->click('//aside[1]/section/ul/li/a[@href="/site/logout"]');
    }

    public function clickOnAsideLogin()
    {
        $I = $this;
        $I->click('//aside[1]/section/ul/li/a[@href="/site/login"]');
    }

    public function seeIAmInLoginFormUi()
    {
        $I = $this;
        $I->seeCurrentUrlEquals('/site/login');
    }

    public function fillLoginForm($user)
    {
        $I = $this;
        $I->fillField('LoginForm[username]', $user['UserRecord[username]']);
        $I->fillField('LoginForm[password]', $user['UserRecord[password]']);
    }

    public function submitLoginForm()
    {
        $I = $this;
        $I->click('//*[@id="login-form"]//button[@type="submit"]');
        $I->wait(2);
    }

    public function seeIamAtHomepage()
    {
        $I = $this;
        $I->seeCurrentUrlEquals('/');
    }

    public function seeUsername($user)
    {
        $I = $this;
        $I->see($user['UserRecord[username]']);
    }

    public function dontSeeUsername($user)
    {
        $I = $this;
        $I->dontSee($user['UserRecord[username]']);
    }
}
