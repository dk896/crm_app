<?php
namespace Step\Acceptance;

use \Codeception\Util\Locator;

class CRMUserSteps extends CRMGuestSteps
{
    public $username = 'JoeUser';
    public $password = '7 wonder @ American soil';

    public function amInQueryCustomerUi()
    {
        $I = $this;
        $I->amOnPage('/customers/query');
    }

    public function fillInPhoneFieldWithDataForm($customer_data)
    {
        $I = $this;
        $I->fillField(
            'phone_number',
            $customer_data['PhoneRecord[number]']
        );
    }

    public function clickSearchButton()
    {
        $I = $this;
        $I->click('Search');
    }

    public function seeIAmInListCustomersUi()
    {
        $I = $this;
        $I->seeCurrentUrlMatches('~customers~');
    }

    public function seeIAmInCustomersResults()
    {
        $I = $this;
        $I->seeCurrentUrlMatches('~customers/results~');
    }

    public function seeCustomerInResults($customer_data)
    {
        $I = $this;
        $I->see(
            $customer_data['CustomerRecord[name]'],
            Locator::elementAt('//*[@id="w0"]/tbody/tr[1]/td', 1)
        );
    }

    public function dontSeeCustomerInResults($customer_data)
    {
        $I = $this;
        $I->dontSee(
            $customer_data['CustomerRecord[name]'],
            Locator::elementAt('//*[@id="w0"]/tbody/tr[1]/td', 1)
        );
    }

    public function amInDocsPage()
    {
        $I = $this;
        $I->amOnPage('/site/docs');
    }

    public function seeTitle()
    {
        $I = $this;
        $I->see('Documentation', '//div/div[1]/section[2]/h1');
    }

    public function seeLargeBodyOfText()
    {
        $I = $this;
        $text = $I->grabTextFrom('//div/div[1]/section[2]/p[1]'); // naive selector
        $I->seeContentIsLong($text);
    }

    // Login
    public function seeIAmInLoginFormUi()
    {
        $I = $this;
        $I->amOnPage('/site/login');
    }
}
