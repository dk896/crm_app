<?php

namespace Step\Acceptance;

use \Codeception\Util\Locator;

class CRMUsersManagementSteps extends CRMGuestSteps
{
    public $username = 'RobAdmin';
    public $password = 'Imitate #14th symptom of apathy';

    const USERS_LIST_SELECTOR = '.grid-view';

    public function amInListUsersUi()
    {
        $I = $this;
        $I->amOnPage('/users');
    }

    public function clickOnUsersListLink()
    {
        $I = $this;
        $I->click('//aside[1]/section/ul/li/a[@href="/users"]');
        $I->wait(2);
    }

    public function seeUsersListLink()
    {
        $I = $this;
        $I->seeLink('Users list', '/users/index');
    }

    public function seeCreateUserLink()
    {
        $I = $this;
        $I->seeLink('Create user record', '/users/create');
    }

    public function seeIamInUsersUi()
    {
        $I = $this;
        $I->seeCurrentUrlEquals('/users');
    }

    public function clickOnCreateUserRecordLink()
    {
        $I = $this;
        $I->click('//aside[1]/section//ul/li/a[@href="/users/create"]');
        $I->wait(2);
    }

    public function clickOnRegisterNewUserButton()
    {
        $I = $this;
        // I click button 'Create User Record'
        $I->click('//div/div[1]/section[2]/div/p/a[@href="/users/create"]');
        $I->wait(2);
    }

    public function seeIamInAddUserUi()
    {
        $I = $this;
        $I->seeCurrentUrlEquals('/users/create');
    }

    public function imagineUser()
    {
        $faker = \Faker\Factory::create();

        return [
            'UserRecord[username]' => $faker->userName,
            'UserRecord[password]' => $faker->password($minLength = 9),
        ];
    }

    public function fillUserDataForm($fieldsData)
    {
        $I = $this;

        foreach ($fieldsData as $key => $value) {
            $I->fillField($key, $value);
        }
    }

    public function submitUserDataForm()
    {
        $I = $this;
        // I click 'button[@type="submit"]' Save
        $I->click('//*[@id="w0"]/div[3]/button[@type="submit"]');
        $I->wait(2);
    }

    public function seeIamInViewUserUi()
    {
        $I = $this;
        $I->seeCurrentUrlMatches('~users/view~'); // regexpr
        $I->see('Update');
        $I->see('Delete');
    }

    public function seeUserInList($user_data)
    {
        $I = $this;
        $I->see($user_data['UserRecord[username]'], self::USERS_LIST_SELECTOR);
    }

    public function seeEditButtonBesideUser($user_data)
    {
        $I = $this;
        $xpath = $this->makeXpathForButtonNearUserName(
            $user_data['UserRecord[username]'],
            'Update'
        );
        $I->seeElement($xpath);
    }

    public function makeXpathForButtonNearUserName(
        $user_name,
        $button_title
    ) {
        $xpath = sprintf(
            '//td[text()="%s"]/following-sibling::td/a[@title="%s"]',
            $user_name,
            $button_title
        );

        return $xpath;
    }

    public function clickEditButtonBesideUser($user_data)
    {
        $I = $this;
        $xpath = $this->makeXpathForButtonNearUserName(
            $user_data['UserRecord[username]'],
            'Update'
        );
        $I->click($xpath);
    }

    public function seeEditUserUi()
    {
        $I = $this;
        $I->seeCurrentUrlMatches('~users/update~');
        $I->see('Update');
    }

    public function seeIAmInListUsersUi()
    {
        $I = $this;
        $I->seeCurrentUrlMatches('~users~');
        $I->seeElement(self::USERS_LIST_SELECTOR);
    }

    public function dontSeeUserInList($user_data)
    {
        $I = $this;
        $I->dontSee(
            $user_data['UserRecord[username]'],
            self::USERS_LIST_SELECTOR
        );
    }

    public function seeDeleteButtonBesideUser($user_data)
    {
        $I = $this;
        $xpath = $this->makeXpathForButtonNearUserName(
            $user_data['UserRecord[username]'],
            'Delete'
        );
        $I->seeElement($xpath);
    }

    public function clickDeleteButtonBesideUser($user_data)
    {
        $I = $this;
        $xpath = $this->makeXpathForButtonNearUserName(
            $user_data['UserRecord[username]'],
            'Delete'
        );
        $I->click($xpath);
        $I->wait(2);
    }

    public function seeDeletionConfirmation()
    {
        $I = $this;
        $I->seeInPopup('delete');
    }

    public function cancelDeletion()
    {
        $I = $this;
        $I->cancelPopup();
    }

    public function confirmDeletion()
    {
        $I = $this;
        $I->acceptPopup();
    }
}
