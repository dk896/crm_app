<?php

use app\models\user\UserRecord;
use \Faker\Factory;
use \yii\base\Security;

class PasswordHashingTest extends \Codeception\Test\Unit
{
    /**
     * @var \FunctionalTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /**
    * Test User`s hash password when saving user
    *
    * @return void
    * */
    public function passwordIsHashedWhenSavingUser()
    {
        $user = $this->imagineUserRecord();

        $plaintext_password = $user->password;
        $user->save();

        $saved_user = UserRecord::findOne($user->id);

        $security = new Security();
        $this->assertInstanceOf(get_class($user), $saved_user);
        $this->assertTrue(
            $security->validatePassword(
                $plaintext_password,
                $saved_user->password
            )
        );
    }

    /**
     *  Test that password is not rehashed after updating without changing password
     *
     * @return void
     */
    public function passwordIsNotRehashedAfterUpdatingWithoutChangingPassword()
    {
        $user = $this->imagineUserRecord();
        $user->save();

        /** @var UserRecord $saved_user */
        $saved_user = UserRecord::findOne($user->id);
        $expected_hash = $saved_user->password;

        $faker = Factory::create();
        $saved_user->username = $faker->userName;
        $saved_user->save();

        /** @var UserRecord $updated_user */
        $updated_user = UserRecord::findOne($saved_user->id);

        $this->assertEquals($expected_hash, $saved_user->password);
        $this->assertEquals($expected_hash, $updated_user->password);
    }



    protected function imagineUserRecord()
    {
        $faker = Factory::create();
        $user = new UserRecord();
        $user->username = $faker->userName;
        $user->password = $faker->password($minLength = 9);

        return $user;
    }
}
