<?php

namespace app\common\fixtures;

use yii\test\ActiveFixture;

class ServiceFixture extends ActiveFixture
{
    public $modelClass = \app\models\service\ServiceRecord::class;
}
