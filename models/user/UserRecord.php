<?php

namespace app\models\user;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\base\NotSupportedException;

// use app\components\MyBehavior;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $password
 */
class UserRecord extends ActiveRecord implements IdentityInterface
{
    /**
     * Returns DB's table name
     *
     * @return string
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [
                [
                    'username',
                    'password',
                    'auth_key',
                ],
                'string',
                'max' => 255
            ],
            [
                [
                    'username',
                    'auth_key',
                ],
                'unique'
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'User name',
            'password' => 'Password',
        ];
    }

    public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);

        if ($this->isAttributeChanged('password')) {
            $this->password = Yii::$app->security->generatePasswordHash(
                $this->password
            );
        }

        if ($this->isNewRecord) {
            $this->auth_key = Yii::$app->security->generateRandomString();
        }

        return $return;
    }

    /**
     * Get current user's ID
     *
     * @return integer|string
     */
    public function getId()
    {
        return $this->id;
    }

    // Required for "Remember me" functionality
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|int $id - the ID to be looked for.
     *
     * @return IdentityInterface|null the identity object that matches the given ID.
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    // Required for "Remember me" validation
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function validatePasswordHash($planeStringPassword)
    {
        return Yii::$app->security->validatePassword($planeStringPassword, $this->password);
    }

    // Useful in case of authorization like OAuth2 or OpenID
    public static function findIdentityByAccessToken($token, $type = null)
    {
        // stub
        throw new NotSupportedException(
            'You can only login by username/password pair for now'
        );
    }

    /**
     * Finds user by username
     *
     * @param string $username - user's name
     *
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }
}
