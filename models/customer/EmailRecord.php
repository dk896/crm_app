<?php

namespace app\models\customer;

use Yii;

/**
 * This is the model class for table "email".
 *
 * @property int $id
 * @property string $purpose
 * @property string $address
 * @property int $customer_id
 *
 * @property CustomerRecord $customer
 */
class EmailRecord extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'email';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id'], 'required'],
            [['customer_id'], 'integer'],
            [['purpose', 'address'], 'string', 'max' => 255],
            [
                ['customer_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => CustomerRecord::className(),
                'targetAttribute' => ['customer_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'purpose' => Yii::t('app', 'Purpose'),
            'address' => Yii::t('app', 'Address'),
            'customer_id' => Yii::t('app', 'Customer ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(
            CustomerRecord::className(),
            ['id' => 'customer_id']
        );
    }

    /**
     * {@inheritdoc}
     * @return EmailQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EmailQuery(get_called_class());
    }
}
