<?php

namespace app\models\customer;

use Yii;

/**
 * This is the model class for table "address".
 *
 * @property int $id
 * @property string $purpose
 * @property string $country
 * @property string $state
 * @property string $city
 * @property string $street
 * @property string $building
 * @property string $apartment
 * @property string $receiver_name
 * @property string $postal_code
 * @property int $customer_id
 *
 * @property CustomerRecord $customer
 */
class AddressRecord extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'address';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id'], 'required'],
            [['customer_id'], 'integer'],
            [
                [
                    'purpose',
                    'country',
                    'state',
                    'city',
                    'street',
                    'building',
                    'apartment',
                    'receiver_name',
                    'postal_code'
                ],
                'string', 'max' => 255
            ],
            [
                ['customer_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => CustomerRecord::className(),
                'targetAttribute' => ['customer_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'purpose' => Yii::t('app', 'Purpose'),
            'country' => Yii::t('app', 'Country'),
            'state' => Yii::t('app', 'State'),
            'city' => Yii::t('app', 'City'),
            'street' => Yii::t('app', 'Street'),
            'building' => Yii::t('app', 'Building'),
            'apartment' => Yii::t('app', 'Apartment'),
            'receiver_name' => Yii::t('app', 'Receiver name'),
            'postal_code' => Yii::t('app', 'Postal code'),
            'customer_id' => Yii::t('app', 'Customer ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(
            CustomerRecord::className(),
            [
                'id' => 'customer_id',
            ]
        );
    }

    /**
     * {@inheritdoc}
     * @return AddressQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AddressQuery(get_called_class());
    }

    /**
     * Return full address like one string
     *
     * @return string
     */
    public function getFullAddress()
    {
        return implode(
            ', ',
            array_filter(
                $this->getAttributes(
                    [
                        'country',
                        'state',
                        'city',
                        'street',
                        'building',
                        'apartment'
                    ]
                )
            )
        );
    }
}
