<?php

namespace app\models\customer;

/**
 * This is the ActiveQuery class for [[EmailRecord]].
 *
 * @see EmailRecord
 */
class EmailQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return EmailRecord[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return EmailRecord|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
