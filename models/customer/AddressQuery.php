<?php

namespace app\models\customer;

/**
 * This is the ActiveQuery class for [[AddressRecord]].
 *
 * @see AddressRecord
 */
class AddressQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return AddressRecord[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return AddressRecord|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
