<?php

return [
    'id' => 'crmapp',
    'basePath' => realpath(__DIR__ . '/../'),
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'bootstrap' => ['debug'],
    'modules' => [
        'debug' => [
            'class' => 'yii\debug\Module',
            'allowedIPs' => ['127.0.*.*']
        ],
        'gii' => [
            'class' => 'yii\gii\Module',
            'allowedIPs' => ['127.0.*.*'],
            'generators' => [
                'crud' => [
                    'class' => 'yii\gii\generators\crud\Generator',
                    'templates' => [
                        'adminlte' => '@app/themes/adminLte/gii/templates/crud/simple',
                    ],
                ],
            ],
        ],
        'api' => [
            'class' => 'app\api\ApiModule',
        ],
    ],
    'components' => [
        'request' => [
            'cookieValidationKey' => 'j0kagr1ejh7iovbha6ertuufdw',
        ],
        'db' => require __DIR__ . '/db.php',
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
        'user' => [
            'identityClass' => 'app\models\user\UserRecord',
            'enableAutoLogin' => true,
            'identityCookie' => [
                'name' => '_identity-app',
                'httpOnly' => true,
            ],
        ],
        'authManager' => [
            'class' => '\yii\rbac\DbManager',
            'defaultRoles' => ['guest'],
        ],
        'session' => [
            'name' => 'app',
        ],
        'view' => [
            'renderers' => [
                'md' => [
                    'class' => 'app\utilities\MarkdownRenderer',
                ]
            ],
            'theme' => [
                'pathMap' => [
                    '@app/views' => '@app/themes/adminLte'
                ],
            ],
        ],
        'assetManager' => [
            'bundles' => (require __DIR__ . '/assets_compressed.php'),
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'extensions' => require __DIR__ . '/../vendor/yiisoft/extensions.php',
];
