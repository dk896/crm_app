<?php
// Set debug mode
// if (!in_array(@$_SERVER['REMOTE_ADDR'], ['127.0.*.*', '127.0.*.*:80', '127.0.0.23:80'])) {
//     die('You are not allowed to access this file.');
// }

defined('YII_DEBUG') or define('YII_DEBUG', true);

// Composer autoload
require __DIR__ . '/../vendor/autoload.php';

// Get Yii. Including the Yii framework itsolf
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

// Override display_errors
ini_set('display_errors', true);

// Get config
$config = require __DIR__ . '/../config/web-test.php';

// Create and call Application with $config
(new yii\web\Application($config))->run();
