<?php
// Set debug mode
defined('YII_DEBUG') or define('YII_DEBUG', true);

// Composer autoload
require __DIR__ . '/../vendor/autoload.php';

// Get Yii. Including the Yii framework itsolf
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

// Override display_errors
ini_set('display_errors', true);

// Get config
$config = require __DIR__ . '/../config/web.php';

// Create and call Application with $config
(new yii\web\Application($config))->run();
