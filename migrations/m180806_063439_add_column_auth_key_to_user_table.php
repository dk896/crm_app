<?php

use yii\db\Migration;

/**
 * Class m180806_063439_add_column_auth_key_to_user_table
 */
class m180806_063439_add_column_auth_key_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'auth_key', 'string UNIQUE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'auth_key');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180806_063439_add_column_auth_key_to_user_table cannot be reverted.\n";

        return false;
    }
    */
}
