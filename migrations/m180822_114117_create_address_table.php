<?php

use yii\db\Migration;

/**
 * Handles the creation of table `address`.
 */
class m180822_114117_create_address_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('address', [
            'id' => $this->primaryKey(),
            'purpose' => $this->string(),
            'country' => $this->string(),
            'state' => $this->string(),
            'city' => $this->string(),
            'street' => $this->string(),
            'building' => $this->string(),
            'apartment' => $this->string(),
            'received_name' => $this->string(),
            'postal_code' => $this->string(),
            'customer_id' => $this->integer()->notNull(),
        ]);

        $this->createIndex(
            'idx_customer_address',
            'address',
            'customer_id'
        );

        // add foreign key with name on table column referencing table column
        $this->addForeignKey(
            'fk_customer_address',
            'address',
            'customer_id',
            'customer',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_customer_address', 'address');
        $this->dropIndex('idx_customer_address', 'address');
        $this->dropTable('address');
    }
}
