<?php

use yii\db\Migration;

/**
 * Class m180822_171314_rename_column_to_receiver_name_in_address_table
 */
class m180822_171314_rename_column_to_receiver_name_in_address_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn(
            'address',
            'received_name',
            'receiver_name'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn(
            'address',
            'receiver_name',
            'received_name'
        );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180822_171314_rename_column_to_receiver_name_in_address_table cannot be reverted.\n";

        return false;
    }
    */
}
