<?php

use yii\db\Migration;

/**
 * Handles the creation of table `email`.
 */
class m180822_140609_create_email_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('email', [
            'id' => $this->primaryKey(),
            'purpose' => $this->string(),
            'address' => $this->string(),
            'customer_id' => $this->integer()->notNull(),
        ]);

        $this->createIndex(
            'idx_customer_email',
            'email',
            'customer_id'
        );

        $this->addForeignKey(
            'fk_customer_email',
            'email',
            'customer_id',
            'customer',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_customer_email', 'email');
        $this->dropIndex('idx_customer_email', 'email');
        $this->dropTable('email');
    }
}
