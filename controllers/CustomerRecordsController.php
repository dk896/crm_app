<?php

namespace app\controllers;

use Yii;
use app\models\customer\CustomerRecord;
use app\models\customer\CustomerRecordSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CustomerRecordController implements the CRUD actions with
 * CustomerRecord model
 */
class CustomerRecordsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists of all CustomerRecord models
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CustomerRecordSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a singl CustomerRecord model
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if model not found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CustomerRecord model.
     * If creation was successful, redirect to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        /** @var ActiveRecord $model */
        $model = new CustomerRecord();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CustomerRecord model.
     * If updates was successful, redirect to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if model not found
     */
    public function actionUpdate($id)
    {
        $this->storeReturnUrl();

        /** @var ActiveRecord $model */
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
                'view',
                'id' => $model->id
            ]);
        }

        return $this->render('update', \compact('model'));
    }

    /**
     * Deletes an existing CustomerRecord model.
     * If deletion was successful, redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if model not found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CustomerRecord model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CustomerRecord the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /** @var ActiveRecord $model */

        if (($model = CustomerRecord::findOne($id)) !== null) {
            return $model;
        }
        
        throw new NotFoundHttpException(
            'The requested page does not exist'
        );
    }

    /**
     * Remember user's requested Url
     * in returnUrl property
     *
     * @return void
     */
    protected function storeReturnUrl()
    {
        Yii::$app->user->returnUrl = Yii::$app->request->url;
    }
}
