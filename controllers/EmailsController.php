<?php

namespace app\controllers;

use app\models\customer\EmailRecord;
use app\utilities\SubmodelController;

/**
 * EmailsController implements the CRUD actions for EmailRecord model.
 */
class EmailsController extends SubmodelController
{
    public $recordClass = EmailRecord::class;
    public $relationAttribute = 'customer_id';
}
