<?php

namespace app\controllers;

use app\models\customer\AddressRecord;
use app\utilities\SubmodelController;

/**
 * AddressesController implements the CRUD actions for AddressRecord model.
 */
class AddressesController extends SubmodelController
{
    public $recordClass = AddressRecord::class;
    public $relationAttribute = 'customer_id';
}
