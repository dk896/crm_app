<?php

namespace app\controllers;

use app\models\customer\PhoneRecord;
use app\utilities\SubmodelController;

/**
 * PhonesController implements the CRUD actions for PhoneRecord model.
 */
class PhonesController extends SubmodelController
{
    public $recordClass = PhoneRecord::class;
    public $relationAttribute = 'customer_id';
}
