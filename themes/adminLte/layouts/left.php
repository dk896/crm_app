<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <?php if(\Yii::$app->user->isGuest): ?>
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/avatar.png" class="img-circle" alt="Guest Image"/>
            </div>
            <div class="pull-left info">
                <p>Guest</p>
            </div>
            <?php else: ?>
                <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/avatar5.png" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= \ucfirst(\Yii::$app->user->identity->username); ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
            <?php endif; ?>
        </div>
        <!-- /.Sidebar user panel -->
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Menu', 'options' => ['class' => 'header']],
                    ['label' => 'Login', 'icon' => 'user-plus', 'url' => ['/site/login'], 'visible' => \Yii::$app->user->isGuest],
                    ['label' => 'Logout', 'icon' => 'hand-o-right', 'url' => ['/site/logout'],
                        'visible' => !\Yii::$app->user->isGuest],
                    [
                        'label' => 'Users',
                        'icon' => 'address-book',
                        'url' => '/users',
                        'visible' => \Yii::$app->user->can('admin'),
                        'items' => [
                            ['label' => 'Users list', 'icon' => 'address-book-o', 'url' => ['/users/index'],],
                            ['label' => 'Create user record', 'icon' => 'address-card-o', 'url' => ['/users/create'],],
                        ],
                    ],
                    [
                        'label' => 'Services',
                        'icon' => 'ship',
                        'url' => '/services',
                        'visible' => \Yii::$app->user->can('manager'),
                        'items' => [
                            ['label' => 'Service list', 'icon' => 'ship', 'url' => ['/services/index'],],
                            ['label' => 'Create service record', 'icon' => 'shopping-cart', 'url' => ['/services/create'],],
                        ],
                    ],
                    [
                        'label' => 'Customers',
                        'icon' => 'users',
                        'url' => '/customers',
                        'visible' => \Yii::$app->user->can('user'),
                        'items' => [
                            ['label' => 'Customers list', 'icon' => 'users', 'url' => ['/customers/index'],],
                            ['label' => 'Add customer', 'icon' => 'handshake-o', 'url' => ['/customers/add'],
                                'visible' => \Yii::$app->user->can('manager'),],
                            ['label' => 'Search customer', 'icon' => 'search', 'url' => ['/customers/query'],],
                        ],
                    ],
                    [
                        'label' => 'Customer Records',
                        'icon' => 'users',
                        'url' => '/customer-records',
                        'visible' => \Yii::$app->user->can('user'),
                    ],
                    [
                        'label' => 'Documentation',
                        'icon' => 'address-book',
                        'url' => ['/site/docs'],
                        'visible' => \Yii::$app->user->can('user'),
                    ],
                    [
                        'label' => 'Gii',
                        'icon' => 'file-code-o',
                        'url' => ['/gii'],
                        'visible' => \Yii::$app->user->can('admin'),
                    ],
                    [
                        'label' => 'Debug',
                        'icon' => 'dashboard',
                        'url' => ['/debug'],
                        'visible' => \Yii::$app->user->can('admin'),
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
