<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Customer Records');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-records-index">

    <?php Pjax::begin(); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Customer Record'), ['create'],
            ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            [
                'attribute' => 'birth_date',
                'format' => ['date', 'php:jS M, Y'],
            ],
            [
                'attribute' => 'country',
                'label' => 'Addresses',
                'format' => 'html',
                'value' => function ($model) {
                    $result = '';
                    foreach ($model->addresses as $address) {
                        $result .= $address->fullAddress . '<br>';
                    }
                    return $result;
                },
            ],
            [
                'attribute' => 'email',
                'label' => 'Emails',
                'format' => 'html',
                'value' => function ($model) {
                    $result = '';
                    foreach ($model->emails as $email) {
                        $result .= $email->address . '<br>';
                    }
                    return $result;
                },
            ],
            [
                'attribute' => 'phone',
                'label' => 'Phones',
                'format' => 'html',
                'value' => function ($model) {
                    $result = '';
                    foreach ($model->phones as $phone) {
                        $result .= $phone->number . '<br>';
                    }
                    return $result;
                },
            ],
            [
                'class' => app\utilities\AuditColumn::class,
                'attribute' => 'id',
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
