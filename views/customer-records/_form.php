<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $model app\models\customer\Customer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-record-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'birth_date')->textInput() ?>

    <?= $form->field($model, 'notes')
        ->textarea(['rows' => 6]) ?>

    <?php if (!$model->isNewRecord) : ?>
        <h2>Phones</h2>
        <?= GridView::widget([
            'dataProvider' => new ActiveDataProvider([
                'query' => $model->getPhones(),
                'pagination' => false,
            ]),
            'columns' => [
                'number',
                [
                    'class' => ActionColumn::class,
                    'controller' => 'phones',
                    'header' => Html::a(
                        '<i class="glyphicon glyphicon-plus"></i>&nbsp;Add new',
                        [
                            'phones/create',
                            'relation_id' => $model->id,
                        ]
                    ),
                    'template' => '{update}{delete}',
                ],
            ],
        ]); ?>
        <h2>Addresses</h2>
        <?= GridView::widget([
            'dataProvider' => new ActiveDataProvider([
                'query' => $model->getAddresses(),
                'pagination' => false,
            ]),
            'columns' => [
                'purpose',
                'country',
                'city',
                'postal_code',
                [
                    'class' => ActionColumn::class,
                    'controller' => 'addresses',
                    'header' => Html::a(
                        '<i class="glyphicon glyphicon-plus"></i>&nbsp;Add new',
                        [
                            'addresses/create',
                            'relation_id' => $model->id,
                        ]
                    ),
                    'template' => '{update}{delete}',
                ],
            ],
        ]); ?>
        <h2>Emails</h2>
        <?= GridView::widget([
            'dataProvider' => new ActiveDataProvider([
                'query' => $model->getEmails(),
                'pagination' => false,
            ]),
            'columns' => [
                'address',
                'purpose',
                [
                    'class' => ActionColumn::class,
                    'controller' => 'emails',
                    'header' => Html::a(
                        '<i class="glyphicon glyphicon-plus"></i>&nbsp;Add new',
                        [
                            'emails/create',
                            'relation_id' => $model->id,
                        ]
                    ),
                    'template' => '{update}{delete}',
                ],
            ],
        ]); ?>
    <?php endif; ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
