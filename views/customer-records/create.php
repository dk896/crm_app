<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\customer\EmailRecord */

$this->title = Yii::t('app', 'Create Customer Record');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Customer Records'),     'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-record-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
