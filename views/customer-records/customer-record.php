<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\customer\CustomerRecord */
/* @var $form ActiveForm */
?>
<div class="customer-record">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'id') ?>
        <?= $form->field($model, 'name') ?>
        <?= $form->field($model, 'birth_date') ?>
        <?= $form->field($model, 'notes') ?>
    
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Submit'),
                ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- customer-record -->
