<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\customer\EmailRecord */

$this->title = Yii::t('app', 'Update Customer Record: ' . $model->name);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Customer Records'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name,
    'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="customer-record-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
