# Documentation

Here we'll see some _Markdown_ code.
It's easier to write text documents with simple formatting this way.

Imagine the user documentation here, describing:

1.  [How to add Customers](/customers/add)
2.  [How to find Customer by phone number](/customers/query)
3.  [How to manage Services](/services/)
4.  [See this documentation](/site/docs)
