<?php

/**
 * @var string $content
 * @var $this yii\web\View
 */
use yii\helpers\Html;
use app\common\widgets\Alert;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

// \yii\bootstrap\BootstrapAsset::register($this);
// \yii\web\YiiAsset::register($this);
app\assets\AppAsset::register($this);
?>
<?php $this->beginPage(); ?>
    <!DOCTYPE html>
    <html lang="<?php echo Yii::$app->language; ?>">
    <head>
        <meta charset="<?php echo Yii::$app->charset; ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title><?php echo Html::encode($this->title); ?></title>
        <?php $this->head(); ?>
        <?php echo Html::csrfMetaTags(); ?>
    </head>
    <body>
        <?php $this->beginBody(); ?>
        <div class="wrap">
            <?php
                NavBar::begin([
                    'brandLabel' => Yii::$app->name,
                    'brandUrl' => Yii::$app->homeUrl,
                    'options' => [
                        'class' => 'navbar-default navbar-fixed-top',
                    ],
                ]);
                $menuItems = [
                    ['label' => 'Customers', 'url' => ['/customers/index']],
                    ['label' => 'Customer add', 'url' => ['/customers/add']],
                    ['label' => 'Customers query', 'url' => ['/customers/query']],
                    ['label' => 'Services', 'url' => ['/services/index']],
                    ['label' => 'Documentation', 'url' => ['/site/docs']],
                    ['label' => 'Gii', 'url' => ['/gii']],
                ];
                /*
                if (Yii::$app->user->isGuest) {
                    $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
                    $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
                } else {
                    $menuItems[] = '<li>'
                    . Html::beginForm(['/site/logout'], 'post')
                    . Html::submitButton(
                        'Logout (' . Yii::$app->user->identity->username . ')',
                        ['class' => 'btn btn-link logout']
                    )
                    . Html::endForm()
                        . '</li>';
                }
                 */
                echo Nav::widget([
                    'options' => ['class' => 'navbar-nav navbar-right'],
                    'items' => $menuItems,
                ]);
                NavBar::end();
            ?>
            <div class="container">
                <?php echo Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]); ?>
                <?php echo Alert::widget(); ?>
                <?php echo $content; ?>
            </div>
        </div>

        <footer class="footer">
            <div class="container">
                <div class="pull-left">
                    &copy; <?php echo 'Kalnynsh D ' . date('Y'); ?>
                </div>
                <div class="pull-right">
                    <?php echo Yii::powered(); ?>
                </div>
            </div>
        </footer>

        <?php $this->endBody(); ?>
    </body>
    </html>
<?php $this->endPage(); ?>
