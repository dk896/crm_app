<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\service\ServiceRecord */

$this->title = Yii::t('app', 'Create Service Record');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Service Records'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="service-record-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
