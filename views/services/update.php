<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\service\ServiceRecord */

$this->title = Yii::t('app', 'Update Service Record: ' . $model->name, [
    'nameAttribute' => '' . $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Service Records'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="service-record-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
