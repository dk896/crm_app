<?php

/**
 * @var  $this yii\web\View
 * @var \yii\data\BaseDataProvider $records
 */

$this->title = 'Customers';
$this->params['breadcrumbs'][] = $this->title;

echo \yii\widgets\ListView::widget(
    [
        'options' => [
            'class' => 'list-view',
            'id' => 'customers_index'
        ],
        'itemView' => '_customer',
        'dataProvider' => $records
    ]
);
