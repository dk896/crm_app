<?php

use \yii\helpers\Html;

$this->title = 'Customers query';
$this->params['breadcrumbs'][] = $this->title;

echo Html::beginForm(['/customers/results'], 'get');

echo Html::label('Phone number to search:', 'phone_number');
echo Html::textInput('phone_number');
echo Html::submitButton('Search');

echo Html::endForm();
