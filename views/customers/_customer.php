<?php

/**
 * @var Customer $model
 */
use app\models\customer\Customer;

echo \yii\widgets\DetailView::widget(
    [
        'model' => $model,
        'attributes' => [
            ['attribute' => 'name'],
            [
                'attribute' => 'birth_date',
                'value' =>
                    (gettype($model->birth_date) === 'string')
                        ? $model->birth_date
                        : $model->birth_date->format('Y-m-d'),
            ],
            'notes:text',
            ['label' => 'phone_number', 'attribute' => 'phones.0.number'],
        ]
    ]
);
