<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\customer\PhoneRecord */

$this->title = Yii::t('app', 'Update Phone Record ID: ' . $model->id, [
    'nameAttribute' => '' . $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Phone Records'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="phone-record-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
