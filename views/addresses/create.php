<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\customer\AddressRecord */

$this->title = Yii::t('app', 'Create Address Record');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Address Records'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="address-record-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
