<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\user\UserRecord */

$this->title = Yii::t('app', 'Create User Record');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Records'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-record-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
