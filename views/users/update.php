<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\user\UserRecord */

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User`s records'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="user-record-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
